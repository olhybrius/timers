import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:timers/liste_minuteurs.dart';
import 'package:flutter_background/flutter_background.dart';

import 'notification_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await NotificationService().init();

  // Since this code is only run at startup, text in the background notification won't
  // be updated even if the locale change. A restart of the app is needed to
  // update the notification content after a locale change.
  final appLocalization = await _getStartupLocalization();
  final androidConfig = FlutterBackgroundAndroidConfig(
    notificationTitle: appLocalization.backgroundNotificationTitle,
    notificationText: appLocalization.backgroundNotificationContent,
    notificationImportance: AndroidNotificationImportance.Default);
  await FlutterBackground.initialize(androidConfig: androidConfig);
  runApp(const MyApp());
}

Future<AppLocalizations> _getStartupLocalization() {
  final preferred = WidgetsBinding.instance.window.locales;
  const supported = AppLocalizations.supportedLocales;
  final locale = basicLocaleListResolution(preferred, supported);
  return AppLocalizations.delegate.load(locale);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        onGenerateTitle: (context) => AppLocalizations.of(context)!.mainTitle,
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),
        home: const TimersList());
  }
}

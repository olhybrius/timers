import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown.dart';
import 'package:flutter_countdown_timer/countdown_controller.dart';
import 'package:timers/timer.dart';
import 'package:timers/utils.dart';

import 'notification_service.dart';

class TimerTile extends StatefulWidget {
  final void Function(Timer, int) onDismissed;
  final Function onChange;
  final Function onPause;
  final Timer item;
  final int index;
  final bool visible;
  final void Function(Timer) onTap;

  const TimerTile(
      {Key? key,
      required this.onDismissed,
      required this.onChange,
      required this.onPause,
      required this.onTap,
      required this.item,
      required this.index,
      required this.visible})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TimerTileState();
}

class _TimerTileState extends State<TimerTile> {
  late CountdownController countdownController;

  @override
  Widget build(BuildContext context) {
    countdownController = CountdownController(
      duration: widget.item.current,
    );

    countdownController.addListener(() {
      if (countdownController.currentDuration == Duration.zero) {
        NotificationService().showNotifications(widget.item.label);
        countdownController.value = widget.item.duration.inMilliseconds;
        countdownController.stop();
        widget.item.isPaused = true;
        setState(() {});
      } else {
        widget.item.current = Duration(
            hours: countdownController.currentRemainingTime.hours ?? 0,
            minutes: countdownController.currentRemainingTime.min ?? 0,
            seconds: countdownController.currentRemainingTime.sec ?? 0);
        widget.onChange(widget.item);
      }
    });
    if (!widget.item.isPaused) countdownController.start();

    return Visibility(
      child: Dismissible(
          key: UniqueKey(),
          dismissThresholds: const {DismissDirection.startToEnd: 0.6},
          background: Container(
              color: Colors.red,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              alignment: AlignmentDirectional.centerStart,
              child: const Icon(Icons.delete, color: Colors.white)),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) {
            widget.onDismissed(widget.item, widget.index);
          },
          child: ListTile(
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(child: Text(widget.item.label)),
                  Countdown(
                    // TODO faire ça propre ??
                    countdownController: countdownController,
                    builder: (
                      BuildContext context,
                      Duration currentRemainingTime,
                    ) {
                      return Text(formatDuration(currentRemainingTime));
                    },
                  ),
                ]),
            trailing: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return ButtonBar(mainAxisSize: MainAxisSize.min, children: [
                IconButton(
                    onPressed: () {
                      if (!countdownController.isRunning) {
                        countdownController.start();
                        widget.item.isPaused = false;
                      } else {
                        countdownController.stop();
                        widget.item.isPaused = true;
                        widget.onPause(widget.item);
                      }
                      setState(() {});
                    },
                    icon: Icon(countdownController.isRunning
                        ? Icons.pause
                        : Icons.play_arrow)),
                IconButton(
                    onPressed: () {
                      countdownController.value =
                          widget.item.duration.inMilliseconds;
                      countdownController.stop();
                      widget.item.isPaused = true;
                      setState(() {});
                    },
                    icon: const Icon(Icons.restart_alt)),
              ]);
            }),
            onTap: () => widget.onTap(widget.item),
          )),
      maintainState: true,
      visible: widget.visible,
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timers/localization.dart';

class NotificationService with Localization{
  //Singleton pattern
  static final NotificationService _notificationService =
      NotificationService._internal();

  factory NotificationService() {
    return _notificationService;
  }

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  NotificationService._internal();

  Future<void> init() async {
    // use default flutter icon for now
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');

    const InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
    );

    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  Future<void> showNotifications(String timerLabel) async {
    var notificationDetails = NotificationDetails(
        android: AndroidNotificationDetails('timers_channel', 'Timers Channel',
            channelDescription: loc.notificationChannelDescription,
            importance: Importance.high,
            fullScreenIntent: true));

    await flutterLocalNotificationsPlugin.show(
      UniqueKey().hashCode,
      loc.mainTitle,
      loc.notificationContent(timerLabel),
      notificationDetails,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:timers/localization.dart';
import 'package:timers/timer.dart';
import 'package:uuid/uuid.dart';

class TimerDetails extends StatefulWidget {
  const TimerDetails({Key? key, required this.notifyParent, this.timer})
      : super(key: key);

  final Timer? timer;
  final Function notifyParent;

  @override
  State<TimerDetails> createState() => _TimerDetailsState();
}

class _TimerDetailsState extends State<TimerDetails> with Localization {
  late TextEditingController labelController;
  int _seconds = 0;
  int _minutes = 0;
  int _hours = 0;

  @override
  void initState() {
    labelController = TextEditingController(text: widget.timer?.label ?? '');
    _seconds = widget.timer?.duration.inSeconds.remainder(60) ?? 0;
    _minutes = widget.timer?.duration.inMinutes.remainder(60) ?? 0;
    _hours = widget.timer?.duration.inHours ?? 0;
    super.initState();
  }

  String _getUuid() {
    if (widget.timer == null) {
      return const Uuid().v4();
    } else {
      return widget.timer!.uuid;
    }
  }

  void _saveTimer() {
    String uuid = _getUuid();
    widget.notifyParent(Timer(uuid, labelController.text,
        Duration(hours: _hours, minutes: _minutes, seconds: _seconds)));
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.timer?.label ?? loc.newTitle),
        actions: [
          IconButton(onPressed: _saveTimer, icon: const Icon(Icons.save))
        ],
      ),
      body: Column(
        children: [
          Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: TextField(
                controller: labelController,
                decoration: InputDecoration(labelText: loc.label),
              )),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            NumberPicker(
                minValue: 0,
                maxValue: 24,
                value: _hours,
                zeroPad: true,
                onChanged: (value) => setState(() => _hours = value)),
            NumberPicker(
                minValue: 0,
                maxValue: 60,
                value: _minutes,
                zeroPad: true,
                onChanged: (value) => setState(() => _minutes = value)),
            NumberPicker(
                minValue: 0,
                maxValue: 60,
                value: _seconds,
                zeroPad: true,
                onChanged: (value) => setState(() => _seconds = value)),
          ])
        ],
      ),
    );
  }
}

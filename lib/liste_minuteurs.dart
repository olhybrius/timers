import 'package:flutter/material.dart';
import 'package:flutter_background/flutter_background.dart';
import 'package:timers/TimerTile.dart';
import 'package:timers/about.dart';
import 'package:timers/persistence.dart';
import 'package:timers/timer.dart';
import 'package:timers/timer_details.dart';

import 'localization.dart';

class TimersList extends StatefulWidget {
  const TimersList({Key? key}) : super(key: key);

  @override
  State<TimersList> createState() => _TimersListState();
}

class _TimersListState extends State<TimersList> with Localization {
  List<Timer> timers = [];
  List<Timer> filteredTimers = [];
  bool searchMode = false;

  @override
  void initState() {
    super.initState();
  }

  void _clearSearch() {
    searchMode = false;
    filteredTimers = [...timers];
  }

  void _addTimer(Timer timer) async {
    await insertTimer(timer);
    setState(() {
      timers.add(timer);
      _clearSearch();
    });
  }

  void _updateTimer(Timer timer) async {
    await updateTimer(timer);
    _updateTimerInList(timer);
  }

  void _updateTimerInList(Timer timer) {
    var timerIndex = timers.indexWhere((element) => element.uuid == timer.uuid);
    setState(() {
      timers[timerIndex] = timer;
    });
  }

  void _deleteTimer(String uuid) async {
    await deleteTimer(uuid);
    setState(() {
      timers.removeWhere((element) => element.uuid == uuid);
      _clearSearch();
    });
  }

  void _reinsertTimer(Timer timer, int index) {
    setState(() {
      timers.insert(index, timer);
      _clearSearch();
    });
  }

  void onDismiss(Timer item, int index) {
    _deleteTimer(item.uuid);
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          content: Text(loc.deletedSnackbar),
          action: SnackBarAction(
            label: loc.cancel,
            onPressed: () {
              _reinsertTimer(item, index);
            },
          )),
    );
  }

  void onTimerTap(Timer item) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                TimerDetails(notifyParent: _updateTimer, timer: item)));
  }

  Future<List<Timer>> _initTimers() async {
    if (timers.isEmpty) {
      timers = await getAllTimers();
      filteredTimers = [...timers];
    }
    bool atLeastOneTimerRunning =
        timers.where((timer) => !timer.isPaused).isNotEmpty;
    if (atLeastOneTimerRunning &&
        !FlutterBackground.isBackgroundExecutionEnabled) {
      await FlutterBackground.enableBackgroundExecution();
    }
    if (!atLeastOneTimerRunning &&
        FlutterBackground.isBackgroundExecutionEnabled) {
      await FlutterBackground.disableBackgroundExecution();
    }
    return timers;
  }

  @override
  Widget build(BuildContext context) {
    Localization.init(context);
    return Scaffold(
      appBar: AppBar(
        title: searchMode
            ? TextField(
                decoration: InputDecoration(
                  prefixIcon: const Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                  hintText: loc.search,
                  hintStyle: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontStyle: FontStyle.italic,
                  ),
                  border: InputBorder.none,
                ),
                style: const TextStyle(
                  color: Colors.white,
                ),
                onChanged: (text) {
                  if (text.isEmpty) {
                    setState(() {
                      _clearSearch();
                    });
                  }
                  final filteredItems = timers
                      .where((element) => element.label
                          .toLowerCase()
                          .contains(text.toLowerCase()))
                      .toList();
                  setState(() {
                    filteredTimers = filteredItems;
                  });
                })
            : Text(loc.mainTitle),
        actions: [
          IconButton(
            onPressed: () {
              if (searchMode) {
                filteredTimers = [...timers];
              }
              setState(() {
                searchMode = !searchMode;
              });
            },
            icon: Icon(searchMode ? Icons.clear : Icons.search),
          ),
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TimerDetails(
                            notifyParent: _addTimer,
                          )),
                );
              },
              icon: const Icon(Icons.add))
        ],
      ),
      body: FutureBuilder(
          future: _initTimers(),
          builder: (context, snapshot) {
            // TODO handle error and loading
            return ListView.separated(
                itemCount: timers.length,
                itemBuilder: (context, index) {
                  return TimerTile(
                      onDismissed: onDismiss,
                      onChange: _updateTimer,
                      onPause: _updateTimerInList,
                      onTap: onTimerTap,
                      item: timers[index],
                      index: index,
                      visible: filteredTimers
                          .map((ft) => ft.uuid)
                          .where((uuid) => uuid == timers[index].uuid)
                          .isNotEmpty);
                },
                separatorBuilder: (BuildContext context, int index) {
                  // Remove top and bottom padding
                  return const Divider(height: 1);
                });
          }),
      drawer: Drawer(
          child: ListView(
        children: [
          const DrawerHeader(
              decoration: BoxDecoration(color: Colors.blue), child: Text("")),
          ListTile(
            leading: const Icon(Icons.info_outline),
            title: Text(loc.about),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const About()));
            },
          )
        ],
      )),
    );
  }
}

class Timer {
  final String uuid;
  final String label;
  final Duration duration;
  Duration current;
  bool isPaused;

  Timer(this.uuid, this.label, this.duration) : current = duration, isPaused = true;

  Map<String, dynamic> toMap() {
    return {
      'uuid': uuid,
      'label': label,
      'duration': duration.inMilliseconds,
    };
  }
}

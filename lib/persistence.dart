import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:timers/timer.dart';

const tableName = 'timers';

Future<Database> _getDbConnection() async {
  return openDatabase(
    join(await getDatabasesPath(), 'database.db'),
    onCreate: (db, version) {
      return _createTable(db);
    },
    version: 1,
  );
}

Future<void> _createTable(Database database) async {
  await database.execute(
      'CREATE TABLE $tableName(uuid TEXT PRIMARY KEY, label TEXT, duration INTEGER)');
}

Future<List<Timer>> getAllTimers() async {
  final db = await _getDbConnection();
  final List<Map<String, dynamic>> maps = await db.query(tableName);
  return List.generate(maps.length, (i) {
    return Timer(
      maps[i]['uuid'],
      maps[i]['label'],
      Duration(milliseconds: maps[i]['duration']),
    );
  });
}

Future<void> insertTimer(Timer timer) async {
  final db = await _getDbConnection();
  await db.insert(tableName, timer.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> updateTimer(Timer timer) async {
  final db = await _getDbConnection();
  await db.update(tableName, timer.toMap(),
      where: 'uuid = ?', whereArgs: [timer.uuid]);
}

Future<void> deleteTimer(String uuid) async {
  final db = await _getDbConnection();
  await db.delete(tableName, where: 'uuid = ?', whereArgs: [uuid]);
}
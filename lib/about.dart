import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:timers/localization.dart';

class About extends StatefulWidget {
  const About({Key? key}) : super(key: key);

  @override
  State<About> createState() => _AboutState();
}

class _AboutState extends State<About> with Localization {
  Future<PackageInfo>? _future;

  @override
  void initState() {
    super.initState();
    _future = _getPackageInfo();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _future,
      builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
        if (snapshot.hasData) {
          var packageInfo = snapshot.data;
          return Scaffold(
              appBar: AppBar(
                title: Text(loc.about),
              ),
              body: ListView(children: [
                ListTile(
                  leading: const Icon(Icons.info_outline),
                  title: Text(loc.version(packageInfo!.appName)),
                  subtitle: Text(
                      "${packageInfo.version} (${packageInfo.buildNumber})"),
                )
              ]));
        } else if (snapshot.hasError) {
          return Text(loc.error(snapshot.error.toString()));
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Future<PackageInfo> _getPackageInfo() async {
    return await PackageInfo.fromPlatform();
  }
}
